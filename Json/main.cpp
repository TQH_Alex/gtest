#include "jsoncpp.cpp"
#include <iostream>
#include <string>
#include <fstream>

typedef bool BOOLEAN;


enum AICB_STATE {
    IDLE,
    BEAM_SELECTED
};

#define PATIENT_ID "PatientId"
#define PLAN_INSTANCE_UID "PlanInstanceUID"
#define BEAM_NUMBER "BeamNumber"
#define CV "CV"
#define RT "RT"
#define RT_REFERENCED_SOP_CLASS_UID "ReferencedSOPClassUID"
#define RT_REFERENCED_SOP_INSTANCE_UID "ReferencedSOPInstanceUID"
#define CV_X "x"
#define CV_Y "y"
#define CV_Z "z"
#define CV_ROT "rot"
#define CV_ROLL "roll"
#define CV_PITCH "pitch"
#define JSON_DATA_MSG_STR "data_msg"


BOOLEAN parseJsonResponseCorrectionMatrix(const std::string& requestJsonSring,
        AICB_STATE& aicbState,
        int& errorCode,
        std::string& description);

BOOLEAN parseJsonResponseCorrectionMatrix(const std::string& requestJsonSring,
        AICB_STATE& aicbState,
        int& errorCode,
        std::string& description)
{
    Json::StyledWriter styledWriter;
    Json::Value root;
    Json::Reader reader;
    Json::Value dataMsg;

    if(!reader.parse(requestJsonSring, root))
    {
        return false;
    }

    dataMsg = root[JSON_DATA_MSG_STR][JSON_DATA_MSG_STR];

    if (!dataMsg.isMember(PATIENT_ID) ||
        !dataMsg.isMember(PLAN_INSTANCE_UID) ||
        !dataMsg.isMember(BEAM_NUMBER) ||
        !dataMsg.isMember(CV) ||
        !dataMsg.isMember(RT))
    {
        return false;
    }

    std::cout << dataMsg;


    return true;
}
int main ()
{
    Json::Value root;
    Json::Reader reader;

    std::ifstream file("correction_matrix_sample.json");
    file >> root;
    Json::FastWriter fastWriter;

    std::string output = fastWriter.write(root);
    AICB_STATE aicbState = IDLE;
    int errorCode = 0;
    std::string description = "";


    BOOLEAN result = parseJsonResponseCorrectionMatrix(output, 
                    aicbState, errorCode, description);

    std::cout << result;

    std::cout << "Hello" << std::endl;
    return 0;
}


