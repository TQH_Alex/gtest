#include <iostream>
#include "list.h"

using namespace std;

int main()
{
    std::vector<int> iList {6,5,4,3,2,1,1,2,3,4,5,6,7};
   

    std::cout << "Hello World! \n";
    BubbleSortList bS;
    cout << "First: ";
    bS.printList(iList);
    bS.sort(iList);
    cout << "Final: ";
    bS.printList(iList);

    return 0;
}
