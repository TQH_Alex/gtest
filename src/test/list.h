#ifndef LIST_H
#define LIST_H

#include <vector>

class List
{
public:
  List();
  ~List();
  void swapNumber(int &iFirstNumber, int &iSecondNumber);
  void printList(std::vector<int> &iList);
private:
  virtual void sort(std::vector<int> &iList) = 0;
};

class BubbleSortList : public List
{
public:
  BubbleSortList();
  virtual ~BubbleSortList();
  void sort(std::vector<int> &iList);

private:
  
};

#endif