#include "list.h"
#include <iostream>
#include <vector>

using namespace std;

void List::swapNumber(int &iFirstNumber, int &iSecondNumber)
{
    iFirstNumber = iFirstNumber ^ iSecondNumber;
    iSecondNumber = iFirstNumber ^ iSecondNumber;
    iFirstNumber = iFirstNumber ^ iSecondNumber;
}

void List::printList(std::vector<int> &iList)
{
    for (int i = 0; i < iList.size(); i++)
    {
        cout << iList[i] << " ";
    }
    cout << endl;
}

void BubbleSortList::sort(std::vector<int> &iList)
{
    for (int i = 0; i < iList.size() - 1; i++)
    {
        for (int j = i + 1; j < iList.size(); j++)
        {
            if (iList[i] > iList[j])
            {
                swapNumber(iList[i], iList[j]);
            }
        }
    }
}

List::List()
{
}

List::~List()
{
}

BubbleSortList::BubbleSortList()
{
}

BubbleSortList::~BubbleSortList()
{
}
