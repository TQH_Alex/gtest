#include <gtest/gtest.h>
#include <iostream>
#include "list.h"
using namespace std;

BubbleSortList bS;
template <typename T>
bool equalVector(const std::vector<T> &v, const std::vector<T> &d)
{
    if (v.size() != d.size())
    {
        return false;
    }
    else
    {
        for (int i = 0; i < v.size(); i++)
        {
            if (v[i] != d[i])
            {
                return false;
            }
        }
    }
    return true;
}

TEST(BubbleSort, IsSorted)
{
    std::vector<int> iListOrgin;
    iListOrgin.push_back(102);
    iListOrgin.push_back(99);
    iListOrgin.push_back(21);
    std::vector<int> iListExpected;
    iListExpected.push_back(21);
    iListExpected.push_back(99);
    iListExpected.push_back(102);
    bS.sort(iListOrgin);

    //EXPECT_EQ(2 + 2, 4);
    ASSERT_TRUE(equalVector(iListExpected, iListOrgin));
}

TEST(MathTest2, TwoPlusOneEqualsThree)
{
    EXPECT_EQ(2 + 1, 3);
    EXPECT_EQ(2 + 2, 4);
    EXPECT_EQ(2 + 3, 7);
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc,argv);

    return RUN_ALL_TESTS();
}