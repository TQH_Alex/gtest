import re

FILE_NAME = 'test_enum.cpp'
FILE_NAME_2 = 'b.c'

ENUM = 'enum'
START_OF_ENUM_CHAR = '{'
END_OF_ENUM_CHAR = '}'
IN_ENUM = ','
EQUAL_IN_ENUN = '='

global CURRENT_ENUM_NUMBER
CURRENT_ENUM_NUMBER = 0

def has_numbers(inputString):
    return bool(re.search(r'\d', inputString))

def extract_number(inputString):
    number  = re.findall(r'[+-]?\d+(?:\.\d+)?', inputString)
    return int(number[0])

def has_comment(inputString):
    return bool(re.search(r'/.*/', inputString))

def clear_comment(inputString):
    """
    """
    if(has_comment(inputString)):
        comment_part = re.findall(r'/.*/', inputString)
        inputString = inputString.replace(comment_part[0], "")
        return inputString
    return inputString

def read_file(file_name):
    try:
        file_out = open(FILE_NAME, "r")
        content = file_out.read()
        lines = content.split("\n")
        return lines 
    except Exception as e:
        raise

def format_string(line, enum_number):
    format_string = "{0: <30} {1:>10}{2:>1}*/".format(line, "/*", enum_number)
    return format_string

def write_file(file_name):
    print 'Hello'

def find_the_correct_enum(lines, current_line, current_enum_number):
    split_line = current_line.strip().split("=")
    for i in xrange(0, len(lines)):
        if split_line[-1] in lines[i] and EQUAL_IN_ENUN not in lines[i] \
        and  has_numbers(lines[i]):
            return extract_number(lines[i])

    return current_enum_number


def is_this_enum(lines):
    start_of_enum = False
    result_content = lines
    current_enum_number = 0
    second_enum = False
    for i in xrange(0, len(lines)):
        if(ENUM in lines[i]):
            start_of_enum = True
        if(start_of_enum and IN_ENUM in lines[i]):
            # Clear the old comment for enum
            result_content[i] = clear_comment(result_content[i])

            if EQUAL_IN_ENUN in lines[i] and has_numbers(lines[i]):
                current_enum_number = extract_number(lines[i])
                result_content[i] = format_string(lines[i], current_enum_number)
                second_enum = True

            elif EQUAL_IN_ENUN in lines[i] and not has_numbers(lines[i]):
                # find the correct current_enum 
                current_enum_number = find_the_correct_enum(result_content, result_content[i], current_enum_number)
                result_content[i] = format_string(lines[i], current_enum_number)
                pass

            else :
                """
                If the enum is the first one, then the current_enum_number 
                will not increase, it will only increase when the enum is the second one
                """
                if (second_enum):
                    current_enum_number = current_enum_number + 1
                result_content[i] = format_string(lines[i], current_enum_number)
                second_enum = True
        elif start_of_enum and START_OF_ENUM_CHAR not in lines[i] \
            and END_OF_ENUM_CHAR not in lines[i] and second_enum and lines[i].strip():
            # Clear the old comment for enum
            result_content[i] = clear_comment(result_content[i])

            #Check again for comment in code
            if result_content[i].strip():
                current_enum_number = current_enum_number + 1
                result_content[i] = format_string(lines[i], current_enum_number)

        if (start_of_enum and END_OF_ENUM_CHAR in lines[i]):
            current_enum_number = 0
            start_of_enum = False
            second_enum = False
    return result_content


def main():
    file_path = 0
    lines = read_file(FILE_NAME)
    result_content = is_this_enum(lines)
    for i in xrange(0, len(result_content)):
        print result_content[i]



if __name__ == '__main__':
    main()