import json
import simplejson
from pprint import pprint
import inspect, os, time
import hashlib



def field_data(field, result, result_detail):
    """Build field data"""
    field_temp = {}
    field_temp['field_label'] = field['field_label']
    field_temp['result'] = result
    field_temp['result_detail'] = result_detail
    return field_temp

def course_data(course_input, index, result, result_detail):
    course_input['patient_id'] = index['patient_id']
    course_input['result'] = ""
    course_input['result_detail'] = ""
    course_input['field_count'] = len(index['fields'])
    course_input['course_name'] = index['course_name']
    return  course_input

def detail_data(data):
    course_name = {}
    course_holder = []
    """Loop through each course"""
    for index in data['courses']:
        field_temp = []
        course_temp = {}

        for field in index['fields']:
            field_temp.append(field_data(field, "", ""))
            course_temp['field'] = field_temp

        course_temp = course_data(course_temp, index, "", "")

        course_holder.append(course_temp)
        course_name['course'] = course_holder
        course_name['course_count'] = len(data['courses'])
    return course_name

def path_to_file(file_name):
    path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) # script directory
    path_to_file = path + "/" + file_name
    return path_to_file

def custom_date():
    print x

def final(file_input_json, pbs_root):
    final = {}
    result_time = time.strftime('%Y_%m_%dT%H_%M_%S')
    data = json.load((open(file_input_json)))
    final['detailed_results'] = detail_data(data)
    final['file_version'] = data['file_version']
    final['sync_time'] = result_time 
    final['schedule_file_creation_time'] = data['file_creation_time']
    final['overall_result'] = ""
    with open("temp.json", 'w') as outfile:
        json.dump(final, outfile, indent = 4, sort_keys = True)

    sha1_hash = ""
    with open("temp.json", 'rb') as f:
        sha1_hash =  hashlib.sha1(f.read()).hexdigest()
    
    file_name_holder = file_input_json.split(".")
    file_name = file_name_holder[0] + '.' + file_name_holder[1] + '.' +  \
                    'result' + '.' + result_time + '.' + sha1_hash + '.json'

    os.rename("temp.json", file_name)

def main():
    file_input_json = 'treatment_schedule_update.2017-08-21T10_50_02.bfab77dd8c725917273a7b03d08116a37d6c71da.json'
    final(file_input_json, "")
    # my code here

if __name__ == "__main__":
    main()


