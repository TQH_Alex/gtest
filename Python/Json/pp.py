import json
import simplejson
from pprint import pprint

data = json.load(open('sample.json'))

final = {}
course_holder = {}
patient_holder = {}


# Check in field of a course
for index in data['courses']:

    field_temp = {}
    field_holder = {}
    course_temp = {}


    for field in index['fields']:
        field_temp[field['field_label']] = (field)
        field_holder.update(field_temp)

    patient_id = index['patient_id']
    course_temp[index['course_name']] = field_holder
    course_temp['last_name'] = index['last_name']
    course_temp['first_name'] = index['first_name']
    course_temp['middle_initial'] = index['middle_initial']

    if patient_id not in patient_holder:
        patient_holder[index['patient_id']] = course_temp
        course_holder.update(patient_holder)
    else:
        # course_holder[patient_id].update(patient_holder)
        patient_holder[index['patient_id']].update(course_temp)

final['patients'] = course_holder

with open('tcs-result.json', 'w') as outfile:
    json.dump(final, outfile, indent = 4, sort_keys = True)