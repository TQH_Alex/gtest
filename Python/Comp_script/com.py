from Tkinter import Text, Tk, Button, END
import re
import json
import collections


r = Tk()
r.geometry("400x400")

# Create text box
t_1 = Text(r, height=10, width=40)
t_2 = Text(r, height=10, width=40)
t_1.focus_set()
t_1.pack()
t_2.pack()

def retrieve_input():
    input_text_1 = t_1.get("1.0",END)
    input_text_2 = t_2.get("1.0",END)
    return input_text_1, input_text_2
'''
Compare two list for same length with different order
'''
compare = lambda x, y: collections.Counter(x) == collections.Counter(y)

def compare_each_element(str1_list, str2_list):
    for i in range(len(str1_list)):
        for j in range(len(str2_list)):
            if str1_list[i] != str2_list[j]:
                print str1_list[i] + '--->' + str2_list[j]
                break

def compare_string(event):
    str1, str2 = retrieve_input()
    str1 = str1.encode("utf-8")
    str2 = str2.encode("utf-8")
    str1_list = re.findall(r"[\w']+", str1)
    str2_list = re.findall(r"[\w']+", str2)

    if str1_list == str2_list:
        print 'It it the same'
        return

    if len(str1_list) == len(str2_list) and \
            compare(str1_list, str2_list):
        print 'List A is equal B with different order'
        compare_each_element(str1_list, str2_list)
        return

    if len(str1_list) == len(str2_list):
        result_list_1 = list(set(str1_list) - set(str2_list))
        result_list_2 = list(set(str2_list) - set(str1_list))
        print 'List A is same length as B with different:', \
                    result_list_1 + result_list_2
        return

    elif len(str1_list) > len(str2_list):
        result_list = list(set(str1_list) - set(str2_list))
        print 'List A is longer with different:', result_list
        return

    else:
        result_list = list(set(str2_list) - set(str1_list))
        print 'List B is longer with different:', result_list
        return

r.bind('<Return>', compare_string)

def highlight_pattern(self, pattern, tag, start="1.0", end="end",
                      regexp=False):
    '''Apply the given tag to all text that matches the given pattern

    If 'regexp' is set to True, pattern will be treated as a regular
    expression according to Tcl's regular expression syntax.
    '''
    start = self.index(start)
    end = self.index(end)
    self.mark_set("matchStart", start)
    self.mark_set("matchEnd", start)
    self.mark_set("searchLimit", end)

    count = tk.IntVar()
    while True:
        index = self.search(pattern, "matchEnd","searchLimit",
                            count=count, regexp=regexp)
        if index == "": break
        if count.get() == 0: break # degenerate pattern which matches zero-length strings
        self.mark_set("matchStart", index)
        self.mark_set("matchEnd", "%s+%sc" % (index, count.get()))
        self.tag_add(tag, "matchStart", "matchEnd")

def focus_next_window(event):
    event.widget.tk_focusNext().focus()
    return("break")

t_1.bind("<Tab>", focus_next_window)
t_2.bind("<Tab>", focus_next_window)

r.mainloop()