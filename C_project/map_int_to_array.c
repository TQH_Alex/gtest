#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NUM_OF_SESSION 4
#define MAX_LEN_OF_STRING 100

static char sessionCurrentString[MAX_NUM_OF_SESSION][MAX_LEN_OF_STRING];

#define TREATMENT_SESSION_STR   "TREATMENT SESSION"
#define PDA_SESSION_STR     "PDA SESSION"
#define QA_SESSION_STR          "QA SESSION"
#define QA_ADMIN_SESSION_STR    "QA_ADMIN SESSION"

typedef enum{
    TREATMENT,
    PDA,
    QA,
    QA_ADMIN

} SESSION;

static const char *currentSession[] = {
    TREATMENT_SESSION_STR, 
    PDA_SESSION_STR, 
    QA_SESSION_STR, 
    QA_ADMIN_SESSION_STR,
};

void map(char **sessionCurrentString, int currentSessionNum)
{
    if (currentSessionNum == TREATMENT)
    {
        strncpy(sessionCurrentString[TREATMENT], TREATMENT_SESSION_STR, sizeof(TREATMENT_SESSION_STR));
        printf("%s\n", "HEHE");
        /* code */
    }
    else if (currentSessionNum == PDA)
    {
        strncpy(sessionCurrentString[PDA], PDA_SESSION_STR, sizeof(PDA_SESSION_STR));
        printf("%s\n", "HEHE");
    }
    else if (currentSessionNum == QA)
    {
        strncpy(sessionCurrentString[PDA], PDA_SESSION_STR, sizeof(QA_SESSION_STR));
    }
    else if (currentSessionNum == QA_ADMIN)
    {
        strncpy(sessionCurrentString[PDA], QA_ADMIN_SESSION_STR, sizeof(QA_ADMIN_SESSION_STR));
    }
}

int main(int argc, char const *argv[])
{
    SESSION currentSessionId = QA;
    char screenTitle[50] = "Patient Selection";
    int currentSessionNum = 1;

    printf("%d\n", QA);
    printf("%s\n", currentSession[currentSessionId]);
    printf("%zu\n", strlen(currentSession[currentSessionId]));

    char* name_with_extension;
    name_with_extension = malloc(strlen(screenTitle)+ strlen(currentSession[currentSessionId])); /* make space for the new string (should check the return value ...) */
    strcpy(name_with_extension, screenTitle); /* copy name into the new var */
    strcat(name_with_extension, currentSession[currentSessionId]); /* add the extension */
    printf("%s\n", name_with_extension);

    return 0;
}