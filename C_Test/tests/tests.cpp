#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../point.h"
#include "../main.c"

struct PointInterface {
   virtual ~PointInterface() {}
   virtual void invokePoint() const { }
};

class MockPoint : public PointInterface {
public:
  MOCK_CONST_METHOD0(invokePoint, void());
};
